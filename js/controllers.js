var imdbApp = angular.module('imdbApp', ['ngRoute']);

// List/search view.
imdbApp.controller('listController', ['$scope', '$http', function ($scope, $http){
  $http.get('./data/imdb250.json').success(function (data){
    $scope.imdbData = data;
  });

  $scope.selectMovie = selectMovie;
}]);


// Gallery view.
imdbApp.controller('galleryController', ['$scope', '$http', function ($scope, $http){
  $http.get('./data/imdb250.json').success(function (data){
    $scope.imdbData = data;
  });

  $scope.query = '';
  $scope.genres = ['All', 'Action', 'Adventure', 'Crime', 'Comedy', 'Drama', 
      'Fantasy', 'Musical', 'Mystery', 'Romance', 'Thriller', 'Western'];

  $scope.selectMovie = selectMovie;
  $scope.setGenre = function (genre){
    $scope.query = genre;
    if (genre == 'All') $scope.query = '';
  }
}]);


// Detail view.
imdbApp.controller('detailController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams){
  $http.get('./data/imdb250.json').success(function (data){
    $scope.imdbData = data;
    $scope.movie = data.filter(function (el){
      return el.imdbID == $routeParams.id;
    })[0];
  });

  $scope.prevDetail = function (rank){
    var numMovies = $scope.imdbData.length;
    var newRank = (rank - 2 + numMovies) % numMovies + 1;
    $scope.movie = $scope.imdbData.filter(function (el){
      return el.rank == newRank;
    })[0];
  };

  $scope.nextDetail = function (rank){
    var numMovies = $scope.imdbData.length;
    var newRank = (rank % numMovies) + 1;
    $scope.movie = $scope.imdbData.filter(function (el){
      return el.rank == newRank;
    })[0];
  };
}]);


function selectMovie (movie){
  window.location.href = '#/movie/'+movie.imdbID;
}
