imdbApp.config(function($routeProvider){
  $routeProvider
      .when('/', {
        templateUrl: './partials/list.html',
        controller: 'listController'
      })
      .when('/gallery', {
        templateUrl: './partials/gallery.html',
        controller: 'galleryController'
      })
      .when('/movie/:id', {
        templateUrl: './partials/details.html',
        controller: 'detailController'
      });
});
